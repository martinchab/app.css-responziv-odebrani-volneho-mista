# CRo README

## Resources

	resources/css/app.css 		- kompilovane a minifikovane styly
	resources/js/app.js			- kompilovane scripty
	resources/js/zombie.js		- css3 kompatibilita pro starsi browsery
	resources/img				- obrazky
	resources/favicons			- favicony
	resources/fonts				- web fonty
	resources/less				- less zdroje
	resources/scripts			- js zdroje


### LESS sources

	resources/less/app.less 	- hlavni styl, konstanty, fonty atd.
	resources/less/layout.less 	- layout
	resources/less/reset.less 	- reset styl
	resources/less/fonts.less 	- fonty
	
	
### GULP

Kompiluje less a scripty (Node v5.*)

	npm install
	gulp