//

// Variables

// --------------------------------------------------
var path = require('path'),
    gulp = require('gulp'),
    watch = require('gulp-watch'),
    less = require('gulp-less'),
    autoprefixer = require('gulp-autoprefixer'),
    pxtorem = require('postcss-pxtorem'),
    postcss = require('gulp-postcss'),
    minifyCss = require('gulp-minify-css'),
    resolveDependencies = require('gulp-resolve-dependencies'),
    uglify = require('gulp-uglifyjs'),
    concat = require('gulp-concat'),
    sourcemaps = require('gulp-sourcemaps'),
	livereload = require('gulp-livereload');

var paths = {
    styles: './www/resources/less/*.less',
    scripts: './www/resources/scripts/**/*.js'
};

//
// Tasks
// --------------------------------------------------

gulp.task('styles', function () {
    var processors = [
        pxtorem({
            root_value: 10,
            replace: false
        })
    ];

    gulp.src('./www/resources/less/app.less')
  
        .pipe(sourcemaps.init())

        .pipe(less({
            paths: [path.join(__dirname, 'less', 'includes')]
        })).on('error', function (err) {
            console.log(err.message);
        })

        .pipe(autoprefixer({
            browsers: ['last 3 versions', 'ie 8', 'ie 9'],
            cascade: false
        }))

        .pipe(postcss(processors))

        .pipe(minifyCss({
            compatibility: 'ie8',
            keepSpecialComments: 1
        }))
  
        .pipe(sourcemaps.write(undefined, { sourceRoot: null }))

        .pipe(gulp.dest('./www/resources/css/'))
	
		.pipe(livereload());

});


gulp.task('scripts', function () {

    gulp.src('./www/resources/scripts/app.js')
        .pipe(resolveDependencies({
            pattern: /\* @requires [\s-]*(.*?\.js)/g
        })).on('error', function(err) {
            console.log(err.message);
        })

        .pipe(concat('app.js'))

        .pipe(uglify('app.js', {
            compress: false
        })).on('error', function (err) {
            console.log(err.message);
        })

        .pipe(gulp.dest('./www/resources/js/'));

    gulp.src(['./www/resources/scripts/css3-mediaqueries.js'])

        .pipe(uglify('zombie.js', {
            compress: false
        })).on('error', function (err) {
            console.log(err.message);
        })
        .pipe(gulp.dest('./www/resources/js/'));

});


//
// Watcher
// --------------------------------------------------

gulp.task('watch', function () {
	livereload.listen();
    gulp.watch(paths.styles, ['styles']);
    gulp.watch(paths.scripts, ['scripts']);
});

//
// Default task & Run
// --------------------------------------------------

gulp.task('default', ['styles', 'scripts', 'watch']);
