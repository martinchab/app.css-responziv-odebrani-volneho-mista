$(function() {
	
	$('.item').on('click touch', function() {
		$('.item').removeClass('active');
		$(this).addClass('active');
		$(this).parent().addClass('active');
	});
	
	$('.item').on('mouseleave', function() {
		$(this).removeClass('active');
		stopVideo();
	});
	
	$('.items').on('mouseleave', function() {
		$('.item').removeClass('active');
		$(this).removeClass('active');
		stopVideo();
	});
	
	$(document).on('click touch', function() {
		if ($(this).closest('.item').length == 0) {
			$('.items, .item').removeClass('active');
		}
	});

	var stopVideo = function() {
		$('iframe').each(function () {
			this.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*')
		});
	};

});
